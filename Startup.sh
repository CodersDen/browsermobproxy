#!/usr/bin/env bash
if ! lsof -t -i:8080
then
	echo port 8080 is free
	echo
	echo ....Starting
	python Base_Controller/BaseController.py
else
	echo port 8080 is not free
	echo
	echo freeing port 8080
	echo
	echo ....Starting
	lsof -i :8080 | tail -1 | awk -F ' ' '{print $2}' | xargs kill -9
	python Base_Controller/BaseConroller.py
fi

# Setting Up Virtual Environment

Use pip to install virtualenv. Once installed create a virtual environment using command `virtualenv <env>`. Once the environment is created, you need to activate the virtual environment. To activate the virtual environment use the command `source <env>/bin/activate`

# Installing dependencies

Install the project dependencies by running the command `pip install -r requirements.txt`

# Installing chromedriver

To install chromedriver run the command `npm install chromedriver`. For more details visit [here]('https://www.npmjs.com/package/chromedriver')

# Setting up browsermob certificate for the respective browser for ssl

You have to explicitly set up the `ca-certificate-rsa.cer` for your respective browser. I have set up the certificate for Chrome browser. Without this the requests cannot be made over secure socket layer. To set up the certificate just open chrome browser goto `settings > advanced > Privacy & Security > Manage Certificates`. If you are working on OSX platform, you will get the `KeyChain Access` window. Just add the `ca-certificate-rsa.cer`. Once added, just click on Trust and select the option `Always Trust` and save the configuration by giving the root password. 

# Executing the script

Change the shell script to executable mode at first by using `chmod +x Startup.sh`. To execute the script just run `./Startup.sh`. The reason this script is created is, even after the server is stopped it occupies the port 8080 causing failure for new server instance to start. So this shell script would free the port `8080` if at all it is occupied and would run the python script.

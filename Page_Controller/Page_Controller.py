#from selenium.webdriver.common.by import By
import time, sys
sys.path.append("../Datalicious/Page_Objects/")
from Page_Objects import Page_Objects

class Page_Controller():
    def starter_kit(self, browser):
        time.sleep(2)
        po.click_on_page_scrollView(browser, "xpath", "//a[contains(text(),'See starter kits')]")
        print "\n>>>Finished execution of starter_kit method"

    def starter_kits_devices(self, browser):
        po.click_on_page_scrollView(browser, "css", "div.parsys.content > div:nth-child(3) > div:nth-child(1) > div > div > div:nth-child(4) > a")
        print "\n>>>Finished execution of starter_kits_devices method"

    def view_product(self, browser):
        po.click_on_page_scrollView(browser, "css", "div#austomation-kit-self-install > div > div > div:nth-child(4) > div > a")
        time.sleep(15)
        print "\n>>>Finished execution of view_product method"

    def add_on(self, browser):
        po.click_on_page_scrollView(browser, "css", "div.nav-button-container > div:nth-child(2) > button[title='Continue']")
        po.click_on_page_scrollView(browser, "css", "div.nav-button-container > div:nth-child(2) > button[type='submit']")
        print "\n>>>Finished execution of add_on method"

    def order_summary(self, browser):
        po.click_element(browser, "css", "input#existingTelstraCustomerNo")
        po.click_element(browser, "css", "select#title > option[value='Mr']")
        po.send_keys(browser, "css", "input#firstName", "Test")
        po.send_keys(browser, "css", "input#lastName", "Test")
        po.click_element(browser, "css", "select[name='dob-day'] > option[value='string:01']")
        po.click_element(browser, "css", "select[name='dob-month'] > option[value='string:01']")
        po.click_element(browser, "css", "select[name='dob-year'] > option[value='number:1999']")
        po.send_keys(browser, "css", "input#mobileNumber", "0412341234")
        po.send_keys(browser, "css", "input#email", "test@test.com.au")
        po.send_keys(browser, "css", "input#confirmEmail", "test@test.com.au")
        po.click_on_page_scrollView(browser, "css", "div.nav-button-container > div:nth-child(2) > button[title='Continue']")
        print "\n>>>Finsihed execution of order_summary method"

    def your_details(self, browser):
        po.send_keys(browser, "css", "div.telstra-address-input > div > div > input", "Arthur")
        time.sleep(5)
        po.click_element(browser, "css", "a[title='ARTHUR AVENUE, BLACKTOWN, NSW, 2148']")
        po.click_element(browser, "css", "select > option[value='240089149']")
        time.sleep(5)
        po.click_on_page_scrollView(browser, "css", "div.nav-button-container > div:nth-child(2) > button[title='Continue']")
        print "\n>>>Finished execution of your_details method"

    def setup_and_delivery(self, browser):
        po.click_element(browser, "css", "select#identificationType > option[value='Passport']")
        po.send_keys(browser, "css", "input#idPassportNumber", "k12341234")
        po.click_element(browser, "css", "select[name='idPassportDateIssue-day'] > option[value='string:01']")
        po.click_element(browser, "css", "select[name='idPassportDateIssue-month'] > option[value='string:01']")
        po.click_element(browser, "css", "select[name='idPassportDateIssue-year'] > option[value='number:2017']")
        po.click_element(browser, "css", "select[name='idDateExpiry-day'] > option[value='string:01']")
        po.click_element(browser, "css", "select[name='idDateExpiry-month'] > option[value='string:01']")
        po.click_element(browser, "css", "select[name='idDateExpiry-year'] > option[value='number:2018']")
        time.sleep(2)
        po.execute_javascript(browser, "window.scrollTo(0, 500)")
        po.click_element(browser, "xpath", "//label[contains(text(),'Owner')]")
        po.send_keys(browser, "css", "input[name='yearsAtCurrentAddress']", "6")
        po.click_element(browser, "css", "select#employmentIndustry > option[value='Business services']")
        po.click_element(browser, "css", "select#employmentOccupation > option[value='Account manager']")
        po.send_keys(browser, "css", "input#employmentName", "Datalicious")
        po.send_keys(browser, "css", "input#employmentPhone", "0412341234")
        po.send_keys(browser, "css", "input[name='yearsWithEmployer']", "6")
        po.send_keys(browser, "css", "input[name='monthsWithEmployer']", "6")
        time.sleep(5)
        po.click_on_page_scrollView(browser, "css", "div.nav-button-container > div:nth-child(2) > button")
        po.click_on_page_scrollView(browser, "css", "div.clearfix > input#terms")
        po.click_on_page_scrollView(browser, "xpath", "//button[@title='Place order securely']")
        time.sleep(5)
        print "\n>>>Finished execution of setup_and_delivery method"

    def get_orderNumber(self, browser):
        orderNumber = po.get_element_by_css(browser, "div#order-panel > input-field:nth-child(2) > fieldset >div > div > div > div:nth-child(2) > div.order-details")
        print "\n>>>Finished execution of get_orderNumber method"
        print "\nOrder number is: "+str(orderNumber.text)

po = Page_Objects()

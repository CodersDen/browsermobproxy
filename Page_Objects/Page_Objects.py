from selenium import webdriver
from selenium.webdriver.common.by import By

class Page_Objects():
    def get_element_by_xpath(self, browser, xpath_element):
        page_object_xpath = browser.find_element(By.XPATH,xpath_element)
        return page_object_xpath

    def get_element_by_css(self, browser, css_element):
        page_object_css = browser.find_element(By.CSS_SELECTOR,css_element)
        return page_object_css

    def scroll_into_view(self, browser,byType,pageObject):
        if byType == "xpath":
            element_to_view = browser.find_element(By.XPATH,pageObject)
            browser.execute_script("arguments[0].scrollIntoView()",element_to_view)
            return element_to_view
        if byType == "css":
            element_to_view = browser.find_element(By.CSS_SELECTOR, pageObject)
            browser.execute_script("arguments[0].scrollIntoView()", element_to_view)
            return element_to_view

    def execute_javascript(self, browser, j_script):
        element = browser.execute_script(j_script)
        return element

    def click_element(self, browser, byType, pageObject):
        if byType == "css":
            element_to_click = browser.find_element(By.CSS_SELECTOR, pageObject)
            element_to_click.click()
        if byType == "xpath":
            element_to_click = browser.find_element(By.XPATH, pageObject)
            element_to_click.click()

    def click_on_page_scrollView(self, browser, byType, pageObject):
        if byType == "css":
            element_to_view = browser.find_element(By.CSS_SELECTOR, pageObject)
            browser.execute_script("arguments[0].scrollIntoView()", element_to_view)
            element_to_view.click()
        if byType == "xpath":
            element_to_view = browser.find_element(By.XPATH, pageObject)
            browser.execute_script("arguments[0].scrollIntoView()", element_to_view)
            element_to_view.click()

    def send_keys(self, browser, byType, pageObject, sendKeys_params):
        if byType == "css":
            browser.find_element(By.CSS_SELECTOR, pageObject).send_keys(sendKeys_params)
        if byType == "xpath":
            browser.find_element(By.XPATH, pageObject).send_keys(sendKeys_params)

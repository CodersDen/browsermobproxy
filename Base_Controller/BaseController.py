import requests, time, json, os, sys
from browsermobproxy import Server
from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.common.exceptions import NoSuchElementException, WebDriverException, ElementNotVisibleException
sys.path.append("../Datalicious/Page_Controller/")
from Page_Controller import Page_Controller

class Base():
    def create_server(self):
        server = Server("../Datalicious/browsermob_proxy/bin/browsermob-proxy")
        server.start()
        return server

    def create_proxy(self, server):
        proxy = server.create_proxy()
        return proxy

    def browser_driver(self, browser_proxy):
        try:
            capabilities = webdriver.DesiredCapabilities().CHROME
            chromeOptions = webdriver.ChromeOptions()
            chromeOptions.add_argument('--proxy-server={host}:{port}'.format(host='localhost', port=browser_proxy.port))
            chromeOptions.add_argument('--ignore-certificate-errors')
            chrome_binary = '/usr/local/bin/chromedriver'
            driver = webdriver.Chrome(executable_path=chrome_binary,chrome_options=chromeOptions)
            return driver
        except WebDriverException as e:
            print "\n>>>Web Driver Exception: {}".format(e)

    def page_interactions(self, browser, server, browser_proxy):
        try:
            browser.set_window_size(1440,900)
            browser.implicitly_wait(10)
            base_url = 'https://www.telstra.com.au/smart-home'
            browser.get(base_url)
            request = requests.get(base_url)
            if request.status_code == 200:
                browser_proxy.new_har('Datalicious')
                # Get the Page Controller class to interact with the web elements
                pc.starter_kit(browser)
                pc.starter_kits_devices(browser)
                pc.view_product(browser)
                pc.add_on(browser)
                pc.order_summary(browser)
                pc.your_details(browser)
                pc.setup_and_delivery(browser)
                pc.get_orderNumber(browser)
                # Dump response as json data
                result = json.dumps(browser_proxy.har, ensure_ascii=False)
                # Create har data
                har_file = open("../Datalicious/Data.har","w")
                har_file.write(str(result))
                har_file.close()
                print "\n>>>Finished execution of Page_interactions method"
                print "\n>>>JSON har file created"
                browser.quit()
                server.stop()
        except NoSuchElementException as e:
            print "\n>>>No Such Element Exception: {}".format(e)
            SystemExit
        except ElementNotVisibleException as e:
            print "\n>>>Element Not Visible Exception: {}".format(e)
            SystemExit

    def read_json_data(self):
        with open('../Datalicious/Data.har') as data_file:
            data = json.load(data_file)
            for ent in data['log']['entries']:
                json_data = str(ent['request']['url'])
                if "https://3603226.fls.doubleclick.net/activityi;dc_pre" in json_data:
                    value = json_data.split(';')
                    print "\n"+str(value)

b = Base()
pc = Page_Controller()

server = b.create_server()
browser_proxy = b.create_proxy(server)
browser = b.browser_driver(browser_proxy)
b.page_interactions(browser, server, browser_proxy)
b.read_json_data()
